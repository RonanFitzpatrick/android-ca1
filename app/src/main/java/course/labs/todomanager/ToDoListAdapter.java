package course.labs.todomanager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.zip.Inflater;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.opengl.Visibility;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ToDoListAdapter extends BaseAdapter  {

    private final List<ToDoItem> mItems = new ArrayList<ToDoItem>();
    private final Context mContext;

    private static final String TAG = "Lab-UserInterface";

    public ToDoListAdapter(Context context) {

        mContext = context;

    }

    public ArrayList<ToDoItem> getList()
    {
        return new ArrayList<ToDoItem>(mItems);
    }

    public void sortByDone()
    {
        Collections.sort(mItems, new Comparator<ToDoItem>()
        {
            public int compare(ToDoItem m1, ToDoItem m2)
            {
                return m1.getStatus().compareTo(m2.getStatus());
            }
        });
    }

    public void sortByPriority()
    {
        Collections.sort(mItems, new Comparator<ToDoItem>()
        {
            public int compare(ToDoItem m1, ToDoItem m2)
            {
                return m2.getPriority().compareTo(m1.getPriority());
            }
        });
    }

    public void sortByDate()
    {
        Collections.sort(mItems, new Comparator<ToDoItem>()
        {
                    public int compare(ToDoItem m1, ToDoItem m2)
                    {
                        return m1.getDate().compareTo(m2.getDate());
                    }
         });
    }

    // Add a ToDoItem to the adapter
    // Notify observers that the data set has changed

    public void add(ToDoItem item) {

        mItems.add(item);
        notifyDataSetChanged();
    }

    // Clears the list adapter of all items.

    public void clear() {

        mItems.clear();
        notifyDataSetChanged();

    }

    // Returns the number of ToDoItems

    @Override
    public int getCount() {

        return mItems.size();

    }

    // Retrieve the number of ToDoItems

    @Override
    public Object getItem(int pos) {

        return mItems.get(pos);

    }

    // Get the ID for the ToDoItem
    // In this case it's just the position

    @Override
    public long getItemId(int pos) {

        return pos;

    }

    // Create a View for the ToDoItem at specified position
    // Remember to check whether convertView holds an already allocated View
    // before created a new View.
    // Consider using the ViewHolder pattern to make scrolling more efficient
    // See: http://developer.android.com/training/improving-layouts/smooth-scrolling.html

    @Override
    public View getView(int position, final View convertView, final ViewGroup parent) {

        final ToDoItem toDoItem = (ToDoItem) this.getItem(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View itemLayout = inflater.inflate(R.layout.todo_item, parent, false);


        setUpPriorityDropDown(itemLayout,toDoItem);
        setUpTitle(itemLayout,toDoItem);
        setUpStatus(itemLayout,toDoItem);
        setUpDate(itemLayout, toDoItem);
        showWarning(itemLayout, toDoItem);

        itemLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ListView listView = (ListView) v.getParent();
                final int position = listView.getPositionForView(v);
                mItems.remove(position);
                notifyDataSetChanged();
                Toast.makeText(mContext, "ToDo item Removed",
                        Toast.LENGTH_LONG).show();
                return false;
            }
        });

        return itemLayout;

    }

    private void showWarning(View itemLayout, ToDoItem toDoItem)
    {
        View v = itemLayout.findViewById(R.id.warning);

        if(toDoItem.getStatus() == ToDoItem.Status.DONE)
        {
            v.setVisibility(View.INVISIBLE);
            return;
        }
        Date future =  DateUtil.addDays(new java.util.Date(), 3);

        if(toDoItem.getDate().before(future))
        {
            v.setVisibility(View.VISIBLE);
            return;
        }
        v.setVisibility(View.INVISIBLE);
        return;
    }

    private void setUpDate(View itemLayout, ToDoItem toDoItem)
    {
        final TextView dateView = (TextView) itemLayout.findViewById(R.id.dateView);
        dateView.setText(toDoItem.getDate().toString());
        dateView.setTextColor(Color.BLACK);
    }

    private void setUpStatus(final View itemLayout, final ToDoItem toDoItem)
    {
        final CheckBox statusView = (CheckBox) itemLayout.findViewById(R.id.statusCheckBox);

        statusView.setChecked(toDoItem.getStatus() == ToDoItem.Status.DONE);
        statusView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                RelativeLayout yourRelLay = (RelativeLayout) buttonView.getParent();

                if(isChecked)
                {
                    yourRelLay.setBackgroundColor(Color.GREEN);
                    toDoItem.setStatus(ToDoItem.Status.DONE);
                    notifyDataSetChanged();
                    showWarning(itemLayout,toDoItem);
                    return;
                }
                toDoItem.setStatus(ToDoItem.Status.NOTDONE);
                yourRelLay.setBackgroundColor(Color.RED);
                showWarning(itemLayout,toDoItem);
            }
        });

        if(toDoItem.getStatus() == ToDoItem.Status.DONE)
        {
            itemLayout.setBackgroundColor(Color.GREEN);
        }
        else
        {
            itemLayout.setBackgroundColor(Color.RED);
        }
    }

    private void setUpTitle(View itemLayout, ToDoItem toDoItem)
    {
        final TextView titleView = (TextView) itemLayout.findViewById(R.id.titleView);
        titleView.setText(toDoItem.getTitle());
        titleView.setTextColor(Color.BLACK);
    }

    public void setUpPriorityDropDown(View itemLayout, final ToDoItem toDoItem)
    {
        final Spinner dropdown = (Spinner)itemLayout.findViewById(R.id.priority);
        ToDoItem.Priority[] items = new ToDoItem.Priority[]{ToDoItem.Priority.LOW, ToDoItem.Priority.MED, ToDoItem.Priority.HIGH};

        final ArrayAdapter<ToDoItem.Priority>adapter = new ArrayAdapter<ToDoItem.Priority>(mContext,
                android.R.layout.simple_spinner_item,items);

        dropdown.setAdapter(adapter);

        dropdown.setSelection(adapter.getPosition(toDoItem.getPriority()));

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
            {
                if(id == 0)
                {
                    toDoItem.setPriority(ToDoItem.Priority.LOW);
                }
                else if(id == 1)
                {
                    toDoItem.setPriority(ToDoItem.Priority.MED);
                }
                else
                {
                    toDoItem.setPriority(ToDoItem.Priority.HIGH);
                }
            }

            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
    }
}
