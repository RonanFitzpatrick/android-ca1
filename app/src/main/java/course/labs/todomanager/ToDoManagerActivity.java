package course.labs.todomanager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import course.labs.todomanager.ToDoItem.Priority;
import course.labs.todomanager.ToDoItem.Status;

public class ToDoManagerActivity extends ListActivity {

	private static final int ADD_TODO_ITEM_REQUEST = 0;
	private static final String FILE_NAME = "TodoManagerActivityData.txt";
	private static final String TAG = "Lab-UserInterface";

	// IDs for menu_layout items
	private static final int MENU_DELETE = Menu.FIRST;
	private static final int MENU_DUMP = Menu.FIRST + 1;
	ToDoListAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mAdapter = new ToDoListAdapter(getApplicationContext());

		getListView().setFooterDividersEnabled(true);

		TextView footerView = (TextView) getLayoutInflater().inflate(R.layout.footer_view,null);

		getListView().addFooterView(footerView);

		footerView.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent myIntent = new Intent(ToDoManagerActivity.this, AddToDoActivity.class);
				ToDoManagerActivity.this.startActivityForResult(myIntent,1);
			}
		});

		getListView().setAdapter(mAdapter);
	}

	public enum Sort {
		PRIORITY, DATE, DONE,
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.menu, menu);

		MenuItem itemSpinner = menu.findItem(R.id.spinner);
		MenuItem itemInfo = menu.findItem(R.id.Info);

		setUpSpinner(itemSpinner);
		setUpInfo(itemInfo);

		return true;
	}

	private void setUpInfo(MenuItem itemInfo)
	{
		itemInfo.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item)
			{
				Intent myIntent = new Intent(ToDoManagerActivity.this, OverView.class);
				myIntent.putParcelableArrayListExtra("List",mAdapter.getList());
				ToDoManagerActivity.this.startActivity(myIntent);
				return true;
			}
		});
	}

	private void setUpSpinner(MenuItem itemSpinner)
	{
		Spinner spinner = (Spinner) MenuItemCompat.getActionView(itemSpinner);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if(id == 0)
				{
					mAdapter.sortByPriority();
				}
				else if(id == 1)
				{
					mAdapter.sortByDate();
				}
				else
				{
					mAdapter.sortByDone();
				}
				mAdapter.notifyDataSetChanged();
			}

			public void onNothingSelected(AdapterView<?> parent)
			{

			}
		});

		Sort[] items = new Sort[]{Sort.PRIORITY, Sort.DATE, Sort.DONE};
		final ArrayAdapter<Sort> adapter = new ArrayAdapter<Sort>(this,
				android.R.layout.simple_spinner_item,items);
		spinner.setAdapter(adapter);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.i(TAG,"Entered onActivityResult()");

		if(resultCode == RESULT_CANCELED)
		{
			return;
		}
		if(resultCode == RESULT_OK)
		{
			ToDoItem toDoItem = new ToDoItem(data);
			mAdapter.add(toDoItem);
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		// Load saved ToDoItems, if necessary

		if (mAdapter.getCount() == 0)
			loadItems();
	}

	@Override
	protected void onPause() {
		super.onPause();

		// Save ToDoItems

		saveItems();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_DELETE:
			mAdapter.clear();
			return true;
		case MENU_DUMP:
			dump();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void dump() {

		for (int i = 0; i < mAdapter.getCount(); i++) {
			String data = ((ToDoItem) mAdapter.getItem(i)).toLog();
			Log.i(TAG,	"Item " + i + ": " + data.replace(ToDoItem.ITEM_SEP, ","));
		}

	}

	private void loadItems() {
		BufferedReader reader = null;
		try {
			FileInputStream fis = openFileInput(FILE_NAME);
			reader = new BufferedReader(new InputStreamReader(fis));

			String title = null;
			String priority = null;
			String status = null;
			Date date = null;

			while (null != (title = reader.readLine()))
			{
				priority = reader.readLine();
				status = reader.readLine();
				date = ToDoItem.FORMAT.parse(reader.readLine());
				mAdapter.add(new ToDoItem(title, Priority.valueOf(priority),
						Status.valueOf(status), date));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			if (null != reader) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// Save ToDoItems to file
	private void saveItems() {
		PrintWriter writer = null;
		try {
			FileOutputStream fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
			writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					fos)));

			for (int idx = 0; idx < mAdapter.getCount(); idx++)
			{
				writer.println(mAdapter.getItem(idx));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != writer) {
				writer.close();
			}
		}
	}
}