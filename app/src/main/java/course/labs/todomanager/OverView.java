package course.labs.todomanager;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class OverView extends Activity {

    private ArrayList<ToDoItem> items;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        items = getIntent().getExtras().getParcelableArrayList("List");

        setContentView(R.layout.activity_over_view);

        if(items.size() > 0)
        {
            SetPercentageOfDone();
            SetNextTaskToDo();
            BreakDownOfHighMediumLow();
            RandomTaskToDoNext();
        }
    }

    private void SetNextTaskToDo()
    {
        Collections.sort(items, new Comparator<ToDoItem>()
        {
            public int compare(ToDoItem m1, ToDoItem m2)
            {
                return m1.getDate().compareTo(m2.getDate());
            }
        });

      TextView view = (TextView)this.findViewById(R.id.nexttasktitle);
      view.setText(items.get(0).getTitle());
        view = (TextView)this.findViewById(R.id.nexttaskdate);
        view.setText(items.get(0).getDate().toString());

        ToDoItem highPriority = null;
        for(ToDoItem i : items)
        {
            if(i.getPriority() == ToDoItem.Priority.HIGH)
            {
                highPriority = i;
                break;
            }
        }
        view = (TextView)this.findViewById(R.id.nexthighprioritytasktitle);
      if (highPriority == null)
      {
          view.setText("You have no high priority tasks to complete!");
          return;
      }
        view.setText(highPriority.getTitle());
        view = (TextView)this.findViewById(R.id.nexthighprioritytaskdate);
        view.setText(highPriority.getDate().toString());
    }

    private void BreakDownOfHighMediumLow()
    {
        int med = 0;
        int high = 0;
        int low = 0;

        for(ToDoItem i : items)
        {
            if(i.getPriority() == ToDoItem.Priority.HIGH)
            {
                high++;
            }
            else if(i.getPriority() == ToDoItem.Priority.LOW)
            {
                low++;
            }
            else if(i.getPriority() == ToDoItem.Priority.MED)
            {
                med++;
            }
        }

        TextView view = (TextView)this.findViewById(R.id.lowtasksleft);
        view.setText(low+"");
        view.setTextColor(Color.GREEN);

        view = (TextView)this.findViewById(R.id.mediumtasksleft);
        view.setText(med +"");
        view.setTextColor(Color.parseColor("#FFA500"));

        view = (TextView)this.findViewById(R.id.hightasksleft);
        view.setText(high+"");
        view.setTextColor(Color.RED);
    }

    private void RandomTaskToDoNext()
    {
        Random rn = new Random();
        int position = rn.nextInt(items.size());

        TextView view = (TextView)this.findViewById(R.id.randomnexttask);

        view.setText(items.get(position).getTitle());
    }

    private void SetPercentageOfDone()
    {
        int done = 0;
        for(ToDoItem i : items)
        {
            if(i.getStatus() == ToDoItem.Status.DONE)
            {
                done++;
            }
        }

        int percent = (int)((done* 100.0f) / items.size() );

        TextView view = (TextView)this.findViewById(R.id.percentdone);

        view.setText("%" + percent);

        ProgressBar bar = (ProgressBar) findViewById(R.id.progressbar);
        bar.setProgress(percent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.overviewmenu, menu);
        MenuItem itemInfo = menu.findItem(R.id.left);
        itemInfo.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                finish();
                return true;
            }
        });
        return true;
    }
}
